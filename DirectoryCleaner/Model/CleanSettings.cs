﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace DirectoryCleaner.Model
{
    /// <summary>
    /// This clean will be de/serialized to disk in the same folder as the one that should be cleaned
    /// </summary>
    public class CleanSettings
    {
        static CleanSettings()
        {
            //just to make sure we always have settings even if a static method or property is referenced
            CleanSettings cs = new CleanSettings();
        }

        public CleanSettings()
        {
            PurgeExtensions = new List<string>();
            Initialize();
        }
        private void Initialize()
        {
            //always make sure there is a default copy of the settings stored with the exe
            if (!File.Exists(CurrentDirectorySettings))
            {
                this.SetupDefaultExtensions();
                WriteCurrentDirectory(this);
            }
        }

        private void SetupDefaultExtensions()
        {
            if (PurgeExtensions == null)
                PurgeExtensions = new List<string>();
            PurgeExtensions.Add("bak");
            PurgeExtensions.Add("err");
            PurgeExtensions.Add("tmp");
        }
        public List<string> PurgeExtensions { get; set; }
#region IO
        public static CleanSettings ReadCurrentDirectory()
        {
            if (!File.Exists(CurrentDirectorySettings))
                return null;
            return CleanSettings.Read(CurrentDirectorySettings);
        }
        const string settingsxmlname = "CleanSettings.config";
        public static CleanSettings Read(string filename)
        {
            if (!File.Exists(filename))
                return null;
            CleanSettings cs = null;
            using (FileStream fs = new FileStream(filename,FileMode.Open))
            {
                XmlSerializer xs = new XmlSerializer(typeof(CleanSettings));
                cs = (CleanSettings)xs.Deserialize(fs);
                fs.Close();
            }
            return cs;
        }

        public static string CurrentDirectorySettings
        {
            get
            {
                string filepath = Path.Combine(CurrentAppDirectory, settingsxmlname);
                return filepath;
            }
        }

        public static string CurrentAppDirectory
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); ;
            }
        }

        public static void WriteCurrentDirectory(CleanSettings cs)
        {
            Write(cs, CurrentDirectorySettings);
        }

        public static void Write(CleanSettings cs, string filename)
        {
            if (cs == null)
                return;
            using (FileStream fs = new FileStream(filename,FileMode.OpenOrCreate))
            {
                XmlSerializer xs = new XmlSerializer(typeof(CleanSettings));
                xs.Serialize(fs, cs);
                fs.Close();
            }
        }
#endregion
    }

}
