﻿using System.IO;
using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace DirectoryCleaner
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static App()
        {
            DispatcherHelper.Initialize();
            ShouldDisplayMessages = true;
            //our default action is to run the settings in the xml file
            ClearFiles();
        }
        static void ClearFiles()
        {
            //load up our settings
            Model.CleanSettings cs = Model.CleanSettings.ReadCurrentDirectory();
            if (cs == null)
                return;
            string curDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string message = string.Empty;
            if (cs.PurgeExtensions != null && cs.PurgeExtensions.Count > 0)
            {
                foreach (var s in cs.PurgeExtensions)
                {
                    DirectoryInfo dir = new DirectoryInfo(curDirectory);
                    FileInfo[] fs = dir.GetFiles("*." + s, SearchOption.AllDirectories);
                    foreach (FileInfo finfo in fs)
                    {
                        File.Delete(finfo.FullName);
                    }
                    message = message + "\n" + "Deleted " + fs.Length + " files with the ." + s + " extension";
                }
            }
            DisplayMessage(message);
        }

        static bool ShouldDisplayMessages { get; set; }
        static void DisplayMessage(string message)
        {
            if (!ShouldDisplayMessages)
                return;
            System.Windows.MessageBox.Show(message);
        }
    }
}
